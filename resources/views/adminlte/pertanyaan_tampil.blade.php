@extends('adminlte.master')

@section('content')
<div class="mt-3 ml-3">
  <div class="card">
    <div class="card-header">
      <h3 class="card-title">Tabel Pertanyaan</h3>
    </div>  
    <div class="card-body">
     @if(session('success'))
       <div class="alert alert-success">
          {{ session('success')}}
        </div>
      @endif
      <a class="btn btn-primary mb-2" href="/pertanyaan/create">Create New Pertanyaan</a>
                <table class="table table-bordered">
                  <thead>                  
                    <tr>
                      <th>No</th>
                      <th>Judul</th>
                      <th>Isi</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                     @forelse($query as $key => $hasil)
                        <tr> 
                            <td> {{ $key + 1 }} </td>
                            <td> {{ $hasil->judul }} </td>
                            <td> {{ $hasil->isi }}</td>
                            <td syle="display: flex;">
                                <a href="/pertanyaan/{{$hasil->id}}" class="btn btn-info btn-sm">detail</a>
                                <a href="/pertanyaan/{{$hasil->id}}/edit" class="btn btn-default btn-sm">edit</a>
                              <form action="/pertanyaan/{{$hasil->id}}" method="post">
                              @csrf
                              @method('DELETE')
                              <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                              </form>
                            </td>
                        </tr>
                        @empty
                         <td colspan="4">No Data</td>
                        @endforelse                      
                    
                  </tbody>
                </table>
      </div>
  </div>
</div>

@endsection