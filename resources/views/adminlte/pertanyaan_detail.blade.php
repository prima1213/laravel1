@extends('adminlte.master')

@section('content')
    <div class="mt-3 ml-3">
        <h4>{{ $query->judul }}</h4>
        <p> {{ $query->isi }} </p>
    </div>
    <a href="/pertanyaan" class="btn btn-info btn-sm">kembali</a>
@endsection