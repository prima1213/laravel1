@extends('adminlte.master')

@section('content')

<!-- /.card -->
<div class="ml-3" mt="3">
            <!-- Horizontal Form -->
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title">Create Pertanyaan</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form class="form-horizontal" method="post" action="/pertanyaan">
              @csrf
                <div class="card-body">
                  <div class="form-group row">
                    <label for="judul_pertanyaan" class="col-sm-2 col-form-label">Judul Pertanyaan</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" name="judul" value="{{ old('judul', '')}}" placeholder="Judul Pertanyan">
                      @error('judul')
                        <div class="alert alert-danger">{{ $message }}</div>
                      @enderror
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="isi_pertanyaan" class="col-sm-2 col-form-label">Pertanyaan</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" name="isi" value="{{ old('isi', '')}}" placeholder="Isi Petanyaan">
                      @error('isi')
                        <div class="alert alert-danger">{{ $message }}</div>
                      @enderror
                    </div>
                  </div>
                  </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-info">Simpan</button>
                </div>
                <!-- /.card-footer -->
              </form>
            </div>
            <!-- /.card -->
</div>
            
@endsection