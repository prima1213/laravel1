<!DOCTYPE html>
<html>
<head>
<title>Tugas1</title>
</head>
<body>

<h1>Buat Account Baru!</h1>


<h3>Sign Up Form</h3>

<form action="/welcome" method="POST">
	@csrf
	<label>First name :</label><br><br>
	<input type="text" name="nama_depan"><br><br>
	<label>Last name :</label><br><br>
	<input type="text" name="nama_belakang"><br><br>
	<label>Gender :</label><br><br>
	<input type="radio" name="gender">Male<br>
	<input type="radio" name="gender">Female<br>
	<input type="radio" name="gender">Other<br><br>
	<label>Nationality</label><br><br>
	<select>
		<option value="Indonesia">Indonesian</option>
		<option value="Malaysia">Malaysian</option>
	</select><br><br>
	<label>Language Spoken:</label><br><br>
	<input type="checkbox" name="bahasa">Bahasa Indonesia<br>
	<input type="checkbox" name="bahasa">Engish<br>
	<input type="checkbox" name="bahasa">Other<br><br>
	<label>Bio</label><br><br>
	<textarea id="biodata" cols="20" rows="8"></textarea><br>
	<button type="submit">Sign Up</button>
</form>

</body>
</html>