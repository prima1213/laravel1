<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function registrasi(){
        return view('form');
    }

    public function welcome(Request $minta){
        $nama_depan = $minta["nama_depan"];
        $nama_belakang = $minta["nama_belakang"];
        return view('home', compact('nama_depan','nama_belakang'));
    }
}
