<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Pertanyaan;
class PertanyaanController extends Controller
{
    public function index(){
        //$query = DB::table('pertanyaan')->get();
        //dd($query);
        $query = Pertanyaan::all();
        return view('adminlte.pertanyaan_tampil', compact('query'));
    }

    public function detail($pertanyaan_id){
        $query = DB::table('pertanyaan')->where('id', $pertanyaan_id)->first();
        //dd($query);
        return view('adminlte.pertanyaan_detail', compact('query'));

    }

    public function create(){
        return view('adminlte.pertanyaan_create');
    }

    public function store(Request $request){
        $request->validate([
          'judul' =>  'required|unique:pertanyaan',
         'isi' => 'required'
        ]);

        //$query = DB::table('pertanyaan')->insert([
          //  "judul" => $request["judul"],
           // "isi" => $request["isi"] 
        //]);
       
        // $pertanyaan = new Pertanyaan;
        //$pertanyaan->judul = $request["judul"];
        //$pertanyaan->isi = $request["isi"];
        //$pertanyaan->save();
            $pertanyaan = Pertanyaan::create([
                "judul" => $request["judul"],
                "isi" => $request["isi"]
            ]);
        return redirect('/pertanyaan')->with('success', 'Pertanyaan berhasil disimpan');
    }

    public function edit($pertanyaan_id){
        $query = DB::table('pertanyaan')->where('id', $pertanyaan_id)->first();
        //dd($query);
        return view('adminlte.pertanyaan_edit', compact('query'));

    }
    public function update($id, Request $request){
        //$query = DB::table('pertanyaan')
                        //->where('id', $id)
                        //->update([
                           // 'judul' => $request['judul'],
                            //'isi' => $request['isi']
                        //]);
            $update = Pertanyaan::where('id', $id)->update([
                "judul" => $request["judul"],
                "isi" => $request["isi"]
            ]);
            return redirect('/pertanyaan')->with('success', 'Berhasil update pertanyaan');
    }

    public function destroy($id){
        //$query = DB::table('pertanyaan')->where('id', $id)->delete();
        Pertanyaan::destroy($id);
        return redirect('/pertanyaan')->with('success', 'Pertanyaan berhasil di hapus');
    }
}
